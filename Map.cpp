#include <Map.hpp>

Map::Map()
{
  m_rs = sf::RectangleShape(sf::Vector2f(BLOC_SZ, BLOC_SZ));

  for(int i=0; i < BLOC_NB_H; i++)
    {
      for(int j=0; j < BLOC_NB_W; j++)
	{
	  m_map[i][j] = TILE_GRASS;
	}
    }
  
  m_colors[TILE_NONE] = sf::Color(150,150,150);
  m_colors[TILE_GRASS] = sf::Color::Green;
  m_colors[TILE_WATER] = sf::Color::Blue;
  m_colors[TILE_SAND] = sf::Color::Yellow;
  m_colors[TILE_FOREST] = sf::Color(25, 100, 25);
}

int Map::getCostTo(int i, int j)
{
  int cost = m_map[i][j];
  if(cost == 0) return MAX_WEIGHT;
  return cost;
}

std::vector<std::pair<int, int> > Map::neighbors(std::pair<int,int> n)
{
  return neighbors(n.first, n.second);
}

std::vector<std::pair<int, int> > Map::neighbors(int i, int j)
{
  std::vector<std::pair<int, int> > vec;

  if(exists(i+1, j)){vec.push_back(std::make_pair(i+1, j));}
  if(exists(i-1, j)){vec.push_back(std::make_pair(i-1, j));}
  if(exists(i, j+1)){vec.push_back(std::make_pair(i, j+1));}
  if(exists(i, j-1)){vec.push_back(std::make_pair(i, j-1));}
  
  return vec;
}

void Map::display(sf::RenderWindow &window)
{

  for(int i=0; i < BLOC_NB_H; i++)
    {
      for(int j=0; j < BLOC_NB_W; j++)
	{
	  m_rs.setPosition( sf::Vector2f(BLOC_SZ*j, BLOC_SZ*i) );
	  m_rs.setFillColor( m_colors[ m_map[i][j] ] );
	  window.draw(m_rs);
	}
    }
}

void Map::setTile(int val, int i, int j)
{
  if( exists(i, j) )
    {
      m_map[i][j] = val;
    }
}

int Map::getTile(int i, int j)
{
  return m_map[i][j];
}

void Map::update()
{
  
}

Map::~Map()
{
  
}
