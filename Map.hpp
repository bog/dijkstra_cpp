/* Map */
#ifndef MAP_HPP
#define MAP_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include <global.hpp>
#include <vector>
#define MAX_WEIGHT 1000
enum
{
  TILE_NONE = 0,
  TILE_GRASS = 1,
  TILE_SAND = 2,
  TILE_FOREST = 3,
  TILE_WATER = 4,

};

class Map
{
public:
  Map();
  void display(sf::RenderWindow &window);
  void update();
  
  void setTile(int val, int i, int j);
  int getTile(int i, int j);
  int getCostTo(int i, int j);
  
  std::vector<std::pair<int, int> > neighbors(int i, int j);
  std::vector<std::pair<int, int> > neighbors(std::pair<int,int> n);
  bool exists(int i, int j)
  {
    return i >= 0 && i < BLOC_NB_H && j >= 0 && j < BLOC_NB_W;
  }
  
  virtual ~Map();
  
protected:
  sf::RectangleShape m_rs;
  int m_map[BLOC_NB_H][BLOC_NB_W];
  sf::Color m_colors[5];
private:
  Map( Map const& map ) = delete;
  Map& operator=( Map const& map ) = delete;
  
};

#endif
