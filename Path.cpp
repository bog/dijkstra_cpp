#include <Path.hpp>
#include <random>
#include <chrono>

Path::Path(Map &map, std::pair<int, int> origin, std::pair<int, int> end) : m_map(map)
						 , m_origin(origin)
						 , m_end(end)
{
  for(int i=0; i<BLOC_NB_H; i++)
    {
      for(int j=0; j<BLOC_NB_W; j++)
	{
	  Node *n = new Node;
	  n->i = i;
	  n->j = j;
	  n->w = MAX_WEIGHT;
	  n->f = nullptr;
	  m_res[i][j] = n;

	  if(origin != std::make_pair(i, j))
	    {
	      m_nodes.push_back( n );   
	    }
	  else
	    {
	      m_nodes.push_front( n );
	    }
	}
    }

  m_nodes[0]->w = 0;

  m_step = sf::RectangleShape(sf::Vector2f(STEP_SZ, STEP_SZ));
}

void Path::display()
{
  for(int i=0; i<BLOC_NB_H; i++)
    {
      for(int j=0; j<BLOC_NB_W; j++)
	{
	  int val = m_res[i][j]->w;
	  std::string m1, m2;
	  
	  if( std::find(m_nodes.begin(),
			m_nodes.end(),
			m_res[i][j]) != m_nodes.end() )
	    {
	      
	      m1 = m2 = " ";
	    }
	  else
	    {
	      m1 = "[";
	      m2 = "]";
	    }
	  
	  if(val == -1)
	    {
	      std::cout<<"  --  ";
	    } 
	  else if(val >= MAX_WEIGHT)
	    {
	      std::cout<<" "<<m1<<"##"<<m2<<" ";
	    }
	  else if (val <= 9)
	    {
	      std::cout<<" "<<m1<<0<<val<<m2 <<" ";
	    }
	  else
	    {
	      std::cout<<" "<<m1<< val <<m2<<" ";
	    }
	  
	  
	}std::cout<<std::endl;
    }
  std::cout<<std::endl;
}

void Path::compute()
{
  m_nodes.clear();
  /* init */
  for(int i=0; i<BLOC_NB_H; i++)
    {
      for(int j=0; j<BLOC_NB_W; j++)
	{
	  m_res[i][j]->w = MAX_WEIGHT;
	  m_res[i][j]->f = nullptr;
	  m_nodes.push_back( m_res[i][j] );
	}
    }
  
  m_res[m_origin.first][m_origin.second]->w = 0;
  compute(m_res[m_origin.first][m_origin.second]);

}

void Path::display(sf::RenderWindow &window)
{
  Node* origin = m_res[m_origin.first][m_origin.second];
  Node* end = m_res[m_end.first][m_end.second];
  Node* current = end;
  
  // steps
  m_step.setFillColor(sf::Color::Red);
  while( current != origin && current->f != nullptr)
    {
      if(current != nullptr && current != end)
	{
	  m_step.setPosition(sf::Vector2f(current->j*BLOC_SZ, current->i*BLOC_SZ));
	  m_step.move(sf::Vector2f((BLOC_SZ - STEP_SZ)/2,
				   (BLOC_SZ-STEP_SZ)/2));
	  window.draw(m_step);
	}
      
      current = current->f;
    }

  // origin
  m_step.setFillColor(sf::Color::Black);
  m_step.setPosition(sf::Vector2f(origin->j*BLOC_SZ, origin->i*BLOC_SZ));
  m_step.move(sf::Vector2f((BLOC_SZ - STEP_SZ)/2,
			   (BLOC_SZ-STEP_SZ)/2));
			   window.draw(m_step);
  // end
  m_step.setFillColor(sf::Color::White);
  m_step.setPosition(sf::Vector2f(end->j*BLOC_SZ, end->i*BLOC_SZ));
  m_step.move(sf::Vector2f((BLOC_SZ - STEP_SZ)/2,
			   (BLOC_SZ-STEP_SZ)/2));
			   window.draw(m_step);
}

void Path::compute(Node* start)
{
  start->w = 0;
  
  while( !m_nodes.empty() )
    {
      auto u_it = std::min_element(m_nodes.begin(), m_nodes.end(), [](Node const* a, Node const* b){
	  return a->w < b->w;
	});
      
      Node* node = *u_it;
      
      for( std::pair<int, int> vp : m_map.neighbors(node->i, node->j) )
	{
	  Node *neighbor = m_res[vp.first][vp.second];
	  int cost = m_map.getCostTo(vp.first, vp.second);

	  bool new_way = false;
	  
	  if( neighbor->w > node->w + cost)
	    {
	      new_way = true;
	    }
	  else if( neighbor->w == node->w + cost)
	    {
	      std::mt19937 r(std::chrono::steady_clock::now().time_since_epoch().count());
	      std::uniform_int_distribution<int> uid(0, 1);

	      new_way = static_cast<bool>(uid(r));
	    }

	  if( new_way )
	    {
	      neighbor->w = node->w + cost;
	      neighbor->f = node;

	    }
	}

      m_nodes.erase(u_it);
      
    }
    
}

Path::~Path()
{
  for(int i=0; i<BLOC_NB_H; i++)
    {
      for(int j=0; j<BLOC_NB_W; j++)
	{
	  delete m_res[i][j];
	}
    }
}
