/* Path */
#ifndef PATH_HPP
#define PATH_HPP
#define STEP_SZ BLOC_SZ/2
#include <iostream>
#include <SFML/Graphics.hpp>
#include <Map.hpp>
#include <deque>
#include <unordered_map>
#include <utility>

struct Node
{
  int i;
  int j;
  int w;
  Node *f;
};

class Path
{
public:
  Path(Map& map, std::pair<int, int> origin, std::pair<int, int> end);
  void compute();
  
  void display();
  void display(sf::RenderWindow &window);

  void setOrigin(int i,int j)
  {
    m_origin = std::make_pair(i, j);
  }

  void setEnd(int i,int j)
  {
    m_end = std::make_pair(i, j);
  }
  
  virtual ~Path();
protected:
  Map &m_map;

  std::deque<Node*> m_nodes;
  std::pair<int, int> m_origin;
  std::pair<int, int> m_end;
  
  Node* m_res[BLOC_NB_H][BLOC_NB_W];
  sf::RectangleShape m_step;

  void compute(Node* node);
  
private:
  Path( Path const& path ) = delete;
  Path& operator=( Path const& path ) = delete;
  
};

#endif
