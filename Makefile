EXEC=dij.out
CXX=clang++
CXXFLAGS=-Wall -g -std=c++11 -I"."
SRC=$(wildcard *.cpp)
OBJ=$(SRC:.cpp=.o)

$(EXEC): $(OBJ)
	$(CXX) $(CXXFLAGS) $^ -lsfml-graphics -lsfml-window -lsfml-system -o  $@

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)
