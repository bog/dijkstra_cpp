## DIJKSTRA CPP IMPLEMENTATION ##

Controles :
<O> Select the origin of the path.
<E> Select the end of the path.
<G> Select grass.
<W> Select water.
<S> Select sand.
<M> Select mountain.
<F> Select forest.
<SPACE> Find a path.

<R> Reset the screen (grass everywhere).
<CTRL-F> Fill the screen with previous selection.

<Mouse-Left> Apply selection (grass/water/sand).
<Mouse-Right> Remove (place grass).

<CTRL-C> Quit.
<CTRL-Q> Quit.
<ESCAPE> Quit.
