#include <iostream>
#include <SFML/Graphics.hpp>
#include <global.hpp>
#include <Map.hpp>
#include <Path.hpp>

int main(int argc, char** argv)
{
  sf::RenderWindow window(sf::VideoMode(BLOC_SZ*BLOC_NB_W, BLOC_SZ*BLOC_NB_H),
			  "Dij");
  sf::Event event;

  Map map;
  Path path(map, std::make_pair(0,0), std::make_pair(BLOC_NB_W-1,BLOC_NB_H-1));
  
  int cursor = TILE_NONE;
  sf::Vector2f mouse_pos;

  sf::Clock compute_clock;
  int compute_delay = 125;

  while( window.isOpen() )
    {
      while( window.pollEvent(event) )
	{
	  switch( event.type )
	    {
	    case sf::Event::Closed: window.close(); break;
	      
	    case sf::Event::KeyPressed:
	      switch( event.key.code )
		{
		case sf::Keyboard::Escape:
		window.close();
		break;
		
		default: break;
		}
	      break;

	    default :break;
	    }
	}
      
      window.clear(sf::Color(4, 139, 154));
      
      // KEYS
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) )
	{
	  if( sf::Keyboard::isKeyPressed(sf::Keyboard::C) )
	    {
	      exit(0);
	    }

	  if( sf::Keyboard::isKeyPressed(sf::Keyboard::Q) )
	    {
	      exit(0);
	    }
	}
      
      // origin
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::O) )
	{
	  cursor = -2;
	}
      // end
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::E) )
	{
	  cursor = -1;
	}

      // tiles
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::M) )
	{
	  cursor = TILE_NONE;
	}
      
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::G) )
	{
	  cursor = TILE_GRASS;
	}
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::W) )
	{
	  cursor = TILE_WATER;
	}
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::S) )
	{
	  cursor = TILE_SAND;
	}

      // compute
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::Space) )
	{
	  if(compute_clock.getElapsedTime().asMilliseconds() >= compute_delay)
	    {
	      path.compute();
	      compute_clock.restart();
	    }
	}

      // reset
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::R) )
	{
	  for(int i=0; i < BLOC_NB_H; i++)
	    {
	      for(int j=0; j < BLOC_NB_W; j++)
		{
		  map.setTile(TILE_GRASS, i, j);
		}
	    }
	}
      //fill
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) )
	{
	  if( sf::Keyboard::isKeyPressed(sf::Keyboard::F) )
	    {
	      for(int i=0; i < BLOC_NB_H; i++)
		{
		  for(int j=0; j < BLOC_NB_W; j++)
		    {
		      map.setTile(cursor, i, j);
		    }
		}
	    }
	}
      else
	{
	  //forest
	  if( sf::Keyboard::isKeyPressed(sf::Keyboard::F) )
	    {
	      cursor = TILE_FOREST;
	    }
	}


      if( sf::Mouse::isButtonPressed(sf::Mouse::Left) )
	{
	  int i = sf::Mouse::getPosition(window).y / BLOC_SZ;
	  int j = sf::Mouse::getPosition(window).x / BLOC_SZ;
	  
	  if( map.exists(i, j) )
	    {
	      // ORIGIN
	      if( cursor == -2 )
		{
		  path.setOrigin(i, j);
		  path.compute();
		}
	      // END
	      else if( cursor == -1 )
		{
		  path.setEnd(i, j);
		  path.compute();
		}
	      // TILE
	      else
		{
		  map.setTile(cursor, i, j);
		}
	    }
	}

      if( sf::Mouse::isButtonPressed(sf::Mouse::Right) )
	{
	  int i = sf::Mouse::getPosition(window).y / BLOC_SZ;
	  int j = sf::Mouse::getPosition(window).x / BLOC_SZ;
	  
	  map.setTile(TILE_GRASS, i, j);
	}
  
      map.update();
      map.display(window);
      path.display(window);
      window.display();
    }
  
  return 0;
}
